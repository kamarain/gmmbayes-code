%GMMB_COMPONENT2CLASS  - transform components into classes in a bayesS struct
function outS = gmmb_component2class(inS);

outS = [];

K = length(inS);

ok = 1;
for k = 1:K
	C = length(inS(k).weight);
	for c = 1:C
		outS(ok).mu = inS(k).mu(:,c);
		outS(ok).sigma = inS(k).sigma(:,:,c);
		outS(ok).weight = 1;
		outS(ok).apriories = inS(k).apriories * inS(k).weight(c);
		ok = ok+1;
	end
end
