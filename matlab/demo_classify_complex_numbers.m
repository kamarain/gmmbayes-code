% DEMO_CLASSIFY_COMPLEX_NUMBERS
%  Demonstrates how the complex numbers can be used with gmmb_
%  functions.
%
%  Original demo (CVS repo): src/matlab/complex-test/demo03.m

fprintf('------------------------------------------\n');
fprintf('This demo generates random points from two\n');
fprintf('complex valued Gaussian distribution,     \n');
fprintf('trains using two GMMs and then uses       \n');
fprintf('Bayesian functionality to classify them.  \n');

ndim = 2;

use_complex = 1;

% mean, std. dev., # of points
trans1 = [3; 2];
rot1 = pi/3;
sd1 = [2; 0.8];
n1 = 2000;

trans2 = [-1; 0];
rot2 = -pi/6;
sd2 = [2.5; 3];
n2 = 1000;

% Lets generate some test data
x1 = [];
for i = 1:ndim
	x1 = [x1 normrnd(0, sd1(i), n1, 1)];
end
x1 = [x1 ones(n1, 1)];
rotm = [ cos(rot1) -sin(rot1) trans1(1);
	sin(rot1) cos(rot1) trans1(2);
	0 0 1 ];
x1 = (rotm * x1')';
x1 = x1(:, 1:2);
t1 = 1*ones(n1, 1);

x2 = [];
for i = 1:ndim
	x2 = [x2 normrnd(0, sd2(i), n2, 1)];
end
x2 = [x2 ones(n2, 1)];
rotm = [ cos(rot2) -sin(rot2) trans2(1);
	sin(rot2) cos(rot2) trans2(2);
	0 0 1 ];
x2 = (rotm * x2')';
x2 = x2(:, 1:2);
t2 = 2*ones(n2, 1);


clf reset
hold on
title('x_1 and x_2 values','FontSize',14);
xlabel('component 1','FontSize',14);
ylabel('component 2','FontSize',14);

% plot data points
if 1
	plot(x1(:,1), x1(:,2), 'rx');
	plot(x2(:,1), x2(:,2), 'bx');
end

data = [x1; x2];
if use_complex
	data = data(:,1) + data(:,2)*1i;
end

%
% Create classifier
%bayesS = newgmmbayes03(data, [t1; t2], [2/3; 1/3]);
bayesS = gmmb_create(data, [t1; t2], 'EM');

% plot estimated normal distributions
drop = 0.2;
if 0
	z1 = [];
	z2 = [];
	x = -10:0.2:10;
	y = -10:0.2:10;
	for i=x
		xy = [i*ones(size(y,2),1) y'];
		z1 = [z1 mvnpdf(xy, bayesS(1).mu, bayesS(1).sigma)];
		z2 = [z2 mvnpdf(xy, bayesS(2).mu, bayesS(2).sigma)];
	end
	mesh(x,y,z1-drop, 1.7*ones(size(z1,1)));
	mesh(x,y,z2-drop);
end

%
% Classify test data
%y = gmmbayes03(bayesS, data);
y = gmmb_classify(bayesS, data);

%
% check error

correctList = y-[t1; t2];
correctTot = length(find(correctList == 0))/(length(t1)+length(t2));

disp([num2str(100*correctTot) '% of the data points was correctly classified.']);

% plot classification errors
if use_complex
	ex = real(data(correctList~=0, 1)');
	ey = imag(data(correctList~=0, 1)');
else
	ex = data(correctList~=0, 1)';
	ey = data(correctList~=0, 2)';
end
if 1
	plot3([ex; ex], [ey; ey], [-drop; 0]*ones(1, size(ex,2)), 'k-');
	plot(ex, ey, 'kx');
end


