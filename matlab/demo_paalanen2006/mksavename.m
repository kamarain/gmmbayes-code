function name = mksavename(desc, index);

[pathstr, fname, ext] = fileparts(desc.datafile);

name = fullfile('results', ...
      ['r-' fname '-' desc.method '-' sprintf('%03d',index) '.mat']);
