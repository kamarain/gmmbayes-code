function [varargout] = anal2(varargin);
[varargout{1:nargout}] = feval(varargin{:});

% usage:
% anal2('create_accs_mat')            to create the accs.mat file used by 'analyze'
% anal2('analyze')


%ind = find_desc_str(descs, 'method', 'EM');
%ind = find_desc_val(descs, 'trainsizes', 0.3);
%ind = find_desc_params(descs, 'Cmax', 8);

% *******************************'

function [] = create_accs_mat();

prefix = 'results/';

% create accs.mat from
% all files under 'prefix'

descs = load_descs(prefix);
accs = load_accuracies(descs);
Cout = load_ccounts(descs);
save('accs', 'accs', 'Cout', 'descs')
return

% ----------------------------------------------------------------------------

function [] = analyze();


load('accs', 'accs', 'Cout', 'descs')

failcounts = sum(accs==-1, 3);
facc = accs;
facc(facc==-1) = 0;

failrate = failcounts ./ size(accs,3);

% discard crashed runs
%avgaccr = sum(facc, 3) ./ (size(facc,3) - failcounts);

% crashed run => zero accuracy
avgaccr = sum(facc, 3) ./ size(facc,3);

maxaccr = max(accs, [], 3);
maxaccr(maxaccr==-1) = 0;

accmat = cat(3, avgaccr, maxaccr);

if 1
	datamask = find_desc_str(descs, 'datafile', '/home/nataliya/TUT/FishView/gmm/gmm_codes/matlab/regression/forestcolors-23d.mat') ~= 0;
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/fail-3d.eps');
end


if 0
% plot all
	datamask = find_desc_str(descs, 'datafile', 'forestcolors-23d.mat') ~= 0;
	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
		{ [1 16] [2 32] [3 64] [4] [5] [6] [7] [8] }, ...
		{ 0.7 0.6 0.5 0.4 0.3 0.2 }, ...
		'figures/');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/Cout-accr.eps');
end


if 0
	datamask = find_desc_str(descs, 'datafile', 'forestcolors-23d.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/forest23d-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/forest23d-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/forest23d-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/forest23d-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/forest23d-fail-3d.eps');
	close all
end

if 0
	datamask = find_desc_str(descs, 'datafile', 'forestcolors-15d.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/forest15d-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/forest15d-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/forest15d-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/forest15d-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/forest15d-fail-3d.eps');
	close all
end

if 0
	datamask = find_desc_str(descs, 'datafile', 'forestcolors-10d.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/forest10d-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/forest10d-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/forest10d-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/forest10d-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/forest10d-fail-3d.eps');
	close all
end

if 0
	datamask = find_desc_str(descs, 'datafile', '../../../data/waveform/wave_noise.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/wavenoise-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/wavenoise-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/wavenoise-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/wavenoise-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/wavenoise-fail-3d.eps');
	close all
end

if 0
	datamask = find_desc_str(descs, 'datafile','/home/nataliya/TUT/FishView/gmm/gmm_data/waveform/wave_noise.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/wave-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/wave-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/wave-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/wave-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/wave-fail-3d.eps');
	close all
end

if 0
	datamask = find_desc_str(descs, 'datafile', '../../../data/letter/letterdata.mat') ~= 0;
%	two_plotter(descs(datamask), accmat(datamask, :, :), failrate(datamask, :), ...
%		{ [8] }, ...
%		{ 0.7 }, ...
%		'figures/letter-');
	Cout_plotter(descs(datamask), Cout(datamask,:, :), accs(datamask, :, :), ...
		'figures/letter-Ca.eps');
	three_plotter(descs(datamask), avgaccr(datamask,:), ...
	   'Average accuracy', 'figures/letter-avgacc-3d.eps');
	three_plotter(descs(datamask), maxaccr(datamask,:), ...
	   'Maximum accuracy', 'figures/letter-maxacc-3d.eps');
	three_plotter(descs(datamask), failrate(datamask,:), ...
	   'Crash rate', 'figures/letter-fail-3d.eps');
	close all
end

% ----------------------------------------------------------------------------

function [] = three_plotter(descs, Q, titl, epsname);

compos = [ 1 2 3 4 5 6 7 8 16 32 64];
%compos = [ 2 4 8 64];

	em_ind =  find_desc_str(descs, 'method', 'EM');
	fj_ind =  find_desc_str(descs, 'method', 'FJ');
	gem_ind = find_desc_str(descs, 'method', 'GEM');


	em_pts = zeros(length(compos), length(descs(1).trainsizes) ) * NaN;
	fj_pts = em_pts;
	gem_pts = em_pts;
	for ci = 1:length(compos)
		c = compos(ci);
		cind = find_desc_params(descs, 'Cmax', c);
		cind = cind | find_desc_params(descs, 'components', c);

		mask = em_ind & cind;
		if any(mask)
			em_pts(ci, :) = Q(mask, :);
		end

		mask = fj_ind & cind;
		if any(mask)
			fj_pts(ci, :) = Q(mask, :);
		end

		mask = gem_ind & cind;
		if any(mask)
			gem_pts(ci, :) = Q(mask, :);
		end
	end


	figH = figure;
	hold on
	p = [];
	trsizes = descs(1).trainsizes*100;

	p(1) = surf(trsizes, compos, em_pts);

	figCA = get(figH, 'CurrentAxes');
	grid on
	set(figCA, 'YScale', 'log', 'YTick', compos);
	set(figCA, 'YMinorTick', 'off', 'YMinorGrid', 'off');
	set(figCA, 'ZLimMode', 'manual', 'ZLim', [0.0 1.0]);

	color = [1 0 0];
	shading flat;
	set(p(1), 'EdgeColor', 0.5*color, 'FaceColor', color);

	hold on

	% FIXME: this is bad hack.
	% I know this whole script is a hack, but this is worse
	%  compos = [ 1 2 3 4 5 6 7 8 16 32 64];
	%compos = [ 2 4 8 64];
	compomask = [4 8 9 10 11];
	%compomask = [2 3 4];

	p(2) = surf(trsizes, compos(compomask), fj_pts(compomask,:));
	color = [0 0 1];
	set(p(2), 'EdgeColor', 0.5*color, 'FaceColor', color);

	p(3) = surf(trsizes, compos(compomask), gem_pts(compomask,:));
	color = [0 1 0];
	set(p(3), 'EdgeColor', 0.5*color, 'FaceColor', color);
	
	set(p, 'FaceAlpha', [0.8], 'EdgeAlpha', [1]);
	ylabel('(max) Components', 'FontSize',14);
	xlabel({'% of data used' 'for training'}, 'FontSize',14);
%	title(titl);
	set(legend(p, 'EM', 'FJ', 'GEM') , 'FontSize',14);
	set(figH, 'Renderer', 'zbuffer');
	%view(-58, 34);
	%view(-109, 36);
	view(123, 24);
	
	save_eps(figH, epsname);
	%print(figH, '-dps', '-r300', '-painters', [epsname '-orig.ps']);
	
	title([titl ' ' epsname]);
	set(figH, 'PaperType', 'A4');
%	print(figH, '-deps2c', '-r300', '-zbuffer', [epsname '.eps']);

	%rotate3d(figCA, 'on');


% ----------------------------------------------------------------------------
function [] = two_plotter(descs, avgaccr, failrate, compolist, trainlist, figprefix);

tlim = [0.0 1.0];
flim = [0.0 1.0];
compos = [ 1 2 3 4 5 6 7 8 16 32 64];

for idx = 1:length(compolist)
	train_plotter(descs, compos, avgaccr, ...
	   compolist{idx}, 'Classification accuracy', ...
	   [figprefix 'accur' sprintf('-%d',compolist{idx})], tlim);

	train_plotter(descs, compos, failrate, ...
	   compolist{idx}, 'Algorithm failure rate', ...
	   [figprefix 'fail' sprintf('-%d',compolist{idx})], flim);
end

for idx = 1:length(trainlist)
	compos_plotter(descs, compos, avgaccr, ...
	   trainlist{idx}, 'Classification accuracy', ...
	   [figprefix 'accur' sprintf('-%02d',trainlist{idx}*10)], tlim);

	compos_plotter(descs, compos, failrate, ...
	   trainlist{idx}, 'Algorithm failure rate', ...
	   [figprefix 'fail' sprintf('-%02d',trainlist{idx}*10)], flim);
end




% ----------------------------------------------------------------------------

function [] = Cout_plotter(descs, Cout, accs, epsname);

Clist = unique(Cout);
Clist = Clist(Clist~=0);

em_ind =  find_desc_str(descs, 'method', 'EM');
fj_ind =  find_desc_str(descs, 'method', 'FJ');
gem_ind = find_desc_str(descs, 'method', 'GEM');

trszcount = size(accs, 2);

em_pts = NaN * ones(length(Clist), trszcount, 3);
fj_pts = em_pts;
gem_pts = em_pts;
max_xi = 0;

for tri = 1:trszcount
	for ci = 1:length(Clist)
		cind =  find_Cout(Cout(:, tri, :), Clist(ci));
		if any(cind)
			max_xi = max(ci, max_xi);
		end
	
		em_pts(ci, tri, :) = minmeanmax(accs(cind & em_ind, tri, :));
		fj_pts(ci, tri, :) = minmeanmax(accs(cind & fj_ind, tri, :));
		gem_pts(ci, tri, :) = minmeanmax(accs(cind & gem_ind, tri, :));
	end
end

figH = figure;
hold on;

tri = 1;  % use the trainsize index 1 = 70% of data

for hihi = 1:3
	p = [];
	legs = {};
	cms = isfinite(em_pts(:, tri, hihi));
	pp = plot(Clist(cms), em_pts(cms, tri, hihi), 'kx-');
	if ~isempty(pp)
		p = [p pp(1)];
		legs = { legs{:} 'EM' };
	end

	cms = isfinite(fj_pts(:, tri, hihi));
	pp = plot(Clist(cms), fj_pts(cms, tri, hihi), 'ko:');
	if ~isempty(pp)
		p = [p pp(1)];
		legs = { legs{:} 'FJ' };
	end

	cms = isfinite(gem_pts(:, tri, hihi));
	pp = plot(Clist(cms), gem_pts(cms, tri, hihi), 'k+--');
	if ~isempty(pp)
		p = [p pp(1)];
		legs = { legs{:} 'GEM' };
	end
end
figCA = get(figH, 'CurrentAxes');
if max_xi ~= 0
	set(figCA, 'XLimMode', 'manual', 'XLim', [0.5 Clist(max_xi)+0.5]);
else
	warning('max_xi is zero.');
end
set(figCA, 'YLimMode', 'manual', 'YLim', [0.0 1.0]);

xlabel('Number of estimated mixture components', 'FontSize',14);
ylabel('Classification accuracy', 'FontSize',14);
%title('Effect of estimated model complexity to classification accuracy');
set(legend(p, legs{:}, 4), 'FontSize',14);

save_eps(figH, epsname);


% ----------------------------------------------------------------------------


function mmm = minmeanmax(M)
	M = M(M~=-1);
%	M = M(:);
%	M(M==-1) = 0;
	if isempty(M)
		mmm = [NaN NaN NaN];
	else
		mmm = [min(M) mean(M) max(M)];
	end


% ----------------------------------------------------------------------------

function figH = train_plotter(descs, compos, Q, curves_compos, titl, fnprefix, ylim);

em_ind =  find_desc_str(descs, 'method', 'EM');
fj_ind =  find_desc_str(descs, 'method', 'FJ');
gem_ind = find_desc_str(descs, 'method', 'GEM');

figH = figure;
if isempty(curves_compos)
	plot_compos = compos;
else
	plot_compos = curves_compos;
end
for c = plot_compos
	cind = find_desc_params(descs, 'Cmax', c);
	cind = cind | find_desc_params(descs, 'components', c);

	hold on
	p = [];
	legs = {};
	
	mask = em_ind & cind;
	if any(mask)
		pts = squeeze(Q(mask, :, :));
		trsz = cat(1, descs(mask).trainsizes);
		pp = plot(trsz, pts, 'kx-');
		p = [p pp(1)];
		legs = { legs{:}, 'EM' };
	end

	mask = fj_ind & cind;
	if any(mask)
		pts = squeeze(Q(mask, :, :));
		trsz = cat(1, descs(mask).trainsizes);
		pp = plot(trsz, pts, 'ko:');
		p = [p pp(1)];
		legs = { legs{:}, 'FJ' };
	end

	mask = gem_ind & cind;
	if any(mask)
		pts = squeeze(Q(mask, :, :));
		trsz = cat(1, descs(mask).trainsizes);
		pp = plot(trsz, pts, 'k+--');
		p = [p pp(1)];
		legs = { legs{:}, 'GEM' };
	end
	
	legend(p, legs{:}, 0);
	
	xlabel('amount of training data');
	title(titl);
	figCA = get(figH, 'CurrentAxes');
	set(figCA, 'YLimMode', 'manual', 'YLim', ylim);

end
save_eps(figH,[fnprefix '-train.eps']);


% ----------------------------------------------------------------------------

function figH = compos_plotter(descs, compos, Q, curves_train, titl, fnprefix, ylim);

em_ind =  find_desc_str(descs, 'method', 'EM');
fj_ind =  find_desc_str(descs, 'method', 'FJ');
gem_ind = find_desc_str(descs, 'method', 'GEM');

% curve count
cc = size(Q, 3);

em_pts{1} = zeros(length(compos), length(descs(1).trainsizes) ) * NaN;
for v = 2:cc
	em_pts{v} = em_pts{1};
end
fj_pts = em_pts;
gem_pts = em_pts;
for ci = 1:length(compos)
	c = compos(ci);
	cind = find_desc_params(descs, 'Cmax', c);
	cind = cind | find_desc_params(descs, 'components', c);

	mask = em_ind & cind;
	if any(mask)
		for v = 1:cc
			em_pts{v}(ci, :) = squeeze(Q(mask, :, v));
		end
	end

	mask = fj_ind & cind;
	if any(mask)
		for v = 1:cc
			fj_pts{v}(ci, :) = squeeze(Q(mask, :, v));
		end
	end

	mask = gem_ind & cind;
	if any(mask)
		for v = 1:cc
			gem_pts{v}(ci, :) = squeeze(Q(mask, :, v));
		end
	end
end


figH = figure;
hold on
	
if isempty(curves_train)
	curves = 1:size(em_pts{1},2);
else
	curves = [];
	for curve_train = curves_train
		curves = [curves find(descs(1).trainsizes == curve_train)];
	end
end

% FIXME: this is bad hack.
% I know this whole script is a hack, but this is worse
%  compos = [ 1 2 3 4 5 6 7 8 16 32 64];
compomask = [4 8 9 10 11];

for v = 1:cc
	a = plot(compos, em_pts{v}(:,curves), 'kx-');
	b = plot(compos(compomask), fj_pts{v}(compomask,curves), 'ko:');
	c = plot(compos(compomask), gem_pts{v}(compomask,curves), 'k+--');
end
figCA = get(figH, 'CurrentAxes');
set(figCA, 'XScale', 'log', 'XTick', compos, 'XMinorTick', 'off');
set(figCA, 'YLimMode', 'manual', 'YLim', ylim);
legend([a(1) b(1) c(1)], 'EM', 'FJ', 'GEM', 0);
xlabel('(max) components');
title(titl);

save_eps(figH,[fnprefix '-compos.eps']);


% ----------------------------------------------------------------------------

function res = load_accuracies(descs)

accmat3d = [];
for i = 1:length(descs)
	fd = load(descs(i).savefile, 'results');
	rescat = cat(1, fd.results{:});
	resmat = cat(1, rescat.rounds);
	
	accmat = [];
	for r = 1:size(resmat, 1)
		accrow = cat(2, resmat{r, :});
		accmat(r, :) = shiftdim(cat(1, accrow.accuracy), -1);
	end
	accmat3d(i, :, :) = shiftdim(accmat, -1);
end
% accmat3d(data_file_index, trainsize_index, repeats_index)

res = accmat3d;

% ----------------------------------------------------------------------------

function res = load_ccounts(descs)

% Note: assumes the 'trainsizes' is always constant

desclen = length(descs);
trainlen = length(descs(1).trainsizes);
itlen = descs(1).redivs * descs(1).iters;

cmat3d = zeros(desclen, trainlen, itlen);
for i = 1:length(descs)
	fd = load(descs(i).savefile, 'results');
	rescat = cat(1, fd.results{:});
	resmat = cat(1, rescat.rounds);
	
	cmat = zeros(trainlen, itlen);
	for r = 1:size(resmat, 1)
		crow = cat(2, resmat{r, :});
		ccol = cat(1, crow.bayesS);
		countmat = zeros(itlen, size(ccol,2));
		for j = 1:prod(size(ccol))
			countmat(j) = size(ccol(j).mu, 2);
		end
		
		if ~isempty(countmat)
			cmat(r, :) = shiftdim(max(countmat,[],2), -1);
		end
	end
	cmat3d(i, :, :) = shiftdim(cmat, -1);
end
% cmat3d(data_file_index, trainsize_index, repeats_index)

res = cmat3d;


% ----------------------------------------------------------------------------

function descs = load_descs(prefix);

files = dir([prefix '*.mat']);

for i = 1:length(files)
	q = load([prefix files(i).name], 'desc');
	descs(i) = q.desc;
end

% ----------------------------------------------------------------------------

function I = find_desc_str(descs, fn, val);

I = zeros(length(descs), 1);

for i = 1:length(descs)
	if strcmp( eval(['descs(' int2str(i) ').' fn]), val )
		I(i) = 1;
	end
end


% ----------------------------------------------------------------------------

function I = find_desc_val(descs, fn, val);

I = zeros(length(descs), 1);

for i = 1:length(descs)
	q = find( eval(['descs(' int2str(i) ').' fn]) == val )
	if ~isempty(q)
		I(i) = q(1);
	end
end

% ----------------------------------------------------------------------------

function I = find_desc_params(descs, fn, val);

I = zeros(length(descs), 1);

for i = 1:length(descs)
	prs = descs(i).params;
	
	% j=1 %%% HACK!! FIXME!! UGLY!!
	%j=2;
	j=1;
	while j < length(prs)
		[p, v] = deal(prs{[j j+1]});
		if strcmp(p, fn) && (v == val)
			I(i) = 1;
			break;
		end
		j = j+2;
	end
end

% ----------------------------------------------------------------------------

function I = find_Cout(Cout, C);

% any of the 15 test runs, any of the training sizes
I = any(any(Cout==C, 3), 2);

