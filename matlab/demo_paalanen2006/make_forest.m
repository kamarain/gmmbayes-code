% According to the article, the best number of neighbour-banks for
% this data set is about 23, which produced classification
% accuracy of 0.86 (values are estimated from Fig. 4a).

% 23 neighbour-banks, accuracy 0.86
% http://www.cs.joensuu.fi/~spectral/databases/download/forest.htm

%@article{795627,
% author = {Joni-Kristian Kamarainen and Ville Kyrki and Jarmo Ilonen and Heikki K\&\#228;lvi\&\#228;inen},
% title = {Improving similarity measures of histograms using smoothing projections},
% journal = {Pattern Recogn. Lett.},
% volume = {24},
% number = {12},
% year = {2003},
% issn = {0167-8655},
% pages = {2009--2019},
% doi = {http://dx.doi.org/10.1016/S0167-8655(03)00039-4},
% publisher = {Elsevier Science Inc.},
% }

clear all
addpath '../../metrics/src'

load '../../../gmm_data/spectral_forest/birch.mat'
load '../../../gmm_data/spectral_forest/pine.mat'
load '../../../gmm_data/spectral_forest/spruce.mat'

rawdata = [birch pine spruce];
class = [1*ones(size(birch,2),1) ; ...
         2*ones(size(pine,2),1) ; ...
	 3*ones(size(spruce,2),1) ...
	];

data = (newneighborbank( size(rawdata, 1), 23, 'KT', 'cos2') * rawdata).';
save('forestcolors-23d.mat', 'data', 'class');

data = (newneighborbank( size(rawdata, 1), 15, 'KT', 'cos2') * rawdata).';
save('forestcolors-15d.mat', 'data', 'class');

data = (newneighborbank( size(rawdata, 1), 10, 'KT', 'cos2') * rawdata).';
save('forestcolors-10d.mat', 'data', 'class');
