function ret = testrun(desc, use_base);

% $Name:  $
% $Id: testrun.m,v 1.3 2004/07/26 13:04:30 paalanen Exp $

disp(sprintf('Testing method %s with', desc.method));
desc.params

if exist(desc.savefile, 'file')
	disp(['file ' desc.savefile ' already exists']);
	ret = 1;
	return;
end

disp(['loading data file ' desc.datafile]);

fdata = load(desc.datafile);

results = {};

for szi = 1:length(desc.trainsizes) % a loop for different training set sizes
	trainport = desc.trainsizes(szi);
	
	sets = {};
	rounds = {};
	
	for i = 1:desc.redivs
		sets{i} = class_divide( ...
		   fdata.class, ...
           [trainport/2 trainport/2 1-trainport]);

		  % [trainport 0.3 0.7-trainport]);
           

		disp(sprintf('running redivision %d of train size %f', i, trainport));
		
		rounds{i} = testround( ...
		   fdata.data, ...
		   fdata.class, ...
		   sets{i}, ...
		   desc.method, ...
		   desc.params, ...
		   desc.iters, ...
		   use_base ...
		   );
	end

	results{szi} = struct( ...
		'sets', {sets}, ...
		'rounds', {rounds}, ...
		'trainsize', {trainport} ...
		);
end


if exist(desc.savefile, 'file')
	disp(['file ' desc.savefile ' already exists']);
	disp('You may now save the results by hand, variables: results, desc');
	ret = 2;
	keyboard
	return
else
	save(desc.savefile, 'results', 'desc');
end

ret = 0;
