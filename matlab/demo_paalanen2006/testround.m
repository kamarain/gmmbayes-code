function ret = testround(data, class, sets, method, params, iters, use_base);

% input variables
%    data   - input samples (N x D)
%    class  - sample labels (N x 1)
%    sets   - data set partitioning (N x 1):
%             1  training set
%             2  test set
%    method - 'EM', 'FJ' or 'GEM'
%    params - method parameters (cell array)
%    iters  - repeat count
%    use_base - are we using the base/ tree (vs. testbase1/ tree)
%
% output variables
%    ret    - bayesS, stats, accuracy
%
% $Name:  $
% $Id: testround.m,v 1.7 2005/01/20 13:02:11 paalanen Exp $
%



train_data = data(sets==1, :);
train_class = class(sets==1, :);

test_data = data(sets==2, :);
test_class = class(sets==2, :);

est = {};
stats = {};
accuracy = {};

for i = 1:iters
	try
		if use_base
			est{i} = gmmb_create(train_data, ...
			   train_class, method, params{:});
			stats{i} = [];
		else
			[est{i} stats{i}] = gmmb_create(train_data, ...
			   train_class, method, params{:});
		end
	catch
		disp('CRASH train');
		est{i} = [];
		stats{i} = lasterr;
		accuracy{i} = -1;
		continue;
	end
	try
		%cresult = gmmb_classify(est{i}, test_data, 'fractile', 0);
%         cresult = my_classify(est{i}, test_data);
%         accuracy{i} = sum(cresult==test_class) / length(test_class);
%         
        for cnt_rounds = 1:5
            cresult = my_classify(est{i}, test_data);
            accuracy_round(cnt_rounds) = sum(cresult==test_class) / length(test_class);
        end
        
        %accuracy{i} = ;
        
	catch
		disp('CRASH classify');
		keyboard
		est{i} = [];
		stats{i} = lasterr;
		accuracy{i} = -1;
	end
end
accuracy
	
ret = struct(...
	'bayesS', est, ...
	'stats', stats, ...
	'accuracy', accuracy ...
	);



function labels = my_classify(bayesS, data);

pdfmat = gmmb_pdf(data, bayesS);

postprob = gmmb_normalize( gmmb_weightprior(pdfmat, bayesS) );

labels = gmmb_decide(postprob);
