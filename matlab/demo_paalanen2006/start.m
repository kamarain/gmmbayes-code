
% $Name:  $
% $Id: start.m,v 1.3 2005/01/20 13:55:57 paalanen Exp $

% for 'require' and friends
addpath('/home/paalanen/work/itlab/itmatlab/funcfunc/');

diary('loki.txt');

% prepare the forest colors data set
make_forest

disp('*************************************************************');
disp('Starting test runs');
disp('');


clear all;

configure;

warning off MATLAB:divideByZero
warning off gmmbayes:gmmb_fj:weight_finity
warning off gmmb_em:data_amount
warning off gmmb_em:fixing_loop

dbstop if error

%load ../../../../data/generated/testdata03.mat
%addpath ../../gmm-test/visuals/

%dbstop in ../../testbase1/gmmb_fj.m at 219
%ret = testrun(tests{2})


for di = 1:length(datafiles)
	desc = testbase;
	desc.datafile = datafiles{di};
	
	for mi = 1:length(methods)
		desc.method = methods{mi};
		indexbase = 0;
		
		for vi = 1:length(varparams{mi}{2})
			desc.params = { solidparams{mi}{:} ...
			     varparams{mi}{1} varparams{mi}{2}{vi} };
			desc.savefile = mksavename(desc, indexbase+vi);

			disp(['Time: ' datestr(now)]);
			
			ret = testrun(desc, use_base);
		end
	end
end



warning on MATLAB:divideByZero
warning on gmmbayes:gmmb_fj:weight_finity
warning on gmmb_em:data_amount
warning on gmmb_em:fixing_loop
