function sets = class_divide(class, parts);
% sets = class_divide(class, parts)
%
% Randomly divide a label vector to disjoint sets
% maintaining class proportions.
%
% input variables:
%    class - vector of integer class labels
%    parts - vector of N set proportions
%
% output variables:
%    sets - vector of set labels 1..N
%
% $Name:  $
% $Id: class_divide.m,v 1.1 2004/07/26 13:03:52 paalanen Exp $


parts = parts ./ sum(parts);
max_part = length(parts);
unks = unique(class);
sets = zeros(size(class));

for nk = 1:length(unks)
	idxs = find( class==unks(nk) );
	idxs = idxs( randperm( length(idxs) ) );

	used = 0;
	for part = 1:(max_part-1)
		n = floor( length(idxs) * parts(part) );
		myidxs = idxs( (used+1):(used+n) );
		sets(myidxs) = part;
		used = used + n;
	end
	myidxs = idxs( (used+1):end );
	sets(myidxs) = max_part;
end

