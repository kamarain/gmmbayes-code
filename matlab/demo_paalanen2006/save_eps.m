function [] = save_eps(figH, file);
% SAVE_EPS - save figure as an Encapsulated Postscript
% save_eps(figure_handle, file_name)

%print(figH, '-Peps', file);
print(figH, '-deps2', '-r600', '-painters', file);
