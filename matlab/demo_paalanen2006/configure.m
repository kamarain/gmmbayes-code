
% $Name:  $
% $Id: configure.m,v 1.7 2010/12/10 10:25:35 paalanen Exp $

% Are we testing the base/ tree?
use_base = 0;

% If not base/ tree, then we are using testbase1/ tree,
% which has additional logging capabilities.

% NOTE: the base3 contains also the extended logging
% capabilities, so the use_base should be 0.
% use_base should really be renamed as extended_logging.

addpath('../base3');

%require('gmmb', 'base3', 'fatal');

% test structure format:
%   datafile - 
%   method   - 'EM', 'FJ' or 'GEM'
%   params   - method specific parameters (cell array)
%   trainsizes - portion of data to use in training, max 0.7, vector
%   redivs   - redivisions of data (class_divide iterations)
%   iters    - iterations with all parameters fixed
%   savefile - file name to save the results, must not exist before
%
% NOTE: the time complexity will be
%   iters * redivs * length(trainsizes) * run(method, params)

%tests{1} = struct( ...
%	'datafile', 'forestcolors.mat', ...
%	'method', 'EM', ...
%	'params', {{'components', 2, 'thr', 1e-5}}, ...
%	'trainsizes', [0.7 0.5 0.4 0.3], ...
%	'redivs', 5, ...
%	'iters', 2, ...
%	'savefile', 'testi.mat' ...
%	);


testbase = struct( ...
	'datafile', '', ...
	'method', '', ...
	'params', {{}}, ...
	'trainsizes', [0.7 0.6 0.5 0.4 0.3 0.2], ...
    'validationsize', 0.5, ...
	'redivs', 20, ...
	'iters', 2, ...
	'savefile', '' ...
	);

 datafiles = { '/home/nataliya/TUT/FishView/gmm/gmm_codes/matlab/regression/forestcolors-23d.mat' ...
           '/home/nataliya/TUT/FishView/gmm/gmm_codes/matlab/regression/forestcolors-15d.mat' }
%              '../../../data/waveform/wave_noise.mat' ...
%datafiles = {	      '../../../data/letter/letterdata.mat' };
%datafiles = {  '/home/nataliya/TUT/FishView/gmm/gmm_data/waveform/wave_noise.mat' };
methods = { 'EM' 'FJ' 'GEM' };
solidparams = { ...
	{  'thr', 1e-5, 'init', 'fcm1' } ...   % methods{1}
	{  'thr', 1e-5 } ...   % methods{2}
	{  'ncand', 8 } ...    % methods{3}
};
varparams = { ...
	{ 'components', { 1 2 3 4 5 6 7 8 } } ... % methods{1}
	{ 'Cmax', { 4 8 16 32 64 } } ... % methods{2}
	{ 'Cmax', { 4 8 16 32 64 } } ... % methods{3}
};


